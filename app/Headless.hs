{-# LANGUAGE OverloadedLists #-}

module Headless where

import RIO

import Engine.Setup (setupHeadless)
import Engine.Types.Options (Options(..))
import Engine.Vulkan.Types qualified as Vulkan
import RIO.App qualified as RIO (App)
import UnliftIO.Resource (MonadResource)
import Vulkan.Core10 qualified as Vk
import Vulkan.Utils.QueueAssignment (QueueFamilyIndex(..))
import VulkanMemoryAllocator qualified as VMA

type App = RIO.App Handles

data Handles = Handles
  { hInstance           :: Vk.Instance
  , hPhysicalDeviceInfo :: Vulkan.PhysicalDeviceInfo
  , hPhysicalDevice     :: Vk.PhysicalDevice
  , hDevice             :: Vk.Device
  , hAllocator          :: VMA.Allocator
  , hQueues             :: Vulkan.Queues (QueueFamilyIndex, Vk.Queue)

  -- XXX: not available in headless
  -- , hWindow             :: GLFW.Window
  -- , hSurface            :: Khr.SurfaceKHR
  -- , hScreenP            :: ProjectionProcess
  -- , hStageSwitch        :: StageSwitchVar
  }

instance Vulkan.HasVulkan Handles where
  getInstance           = hInstance
  getQueues             = hQueues
  getPhysicalDevice     = hPhysicalDevice
  getPhysicalDeviceInfo = hPhysicalDeviceInfo
  getDevice             = hDevice
  getAllocator          = hAllocator

data RenderTarget = RenderTarget
  { surfaceExtent  :: Vk.Extent2D
  , surfaceFormat  :: Vk.Format
  , depthFormat    :: Vk.Format
  , multisample    :: Vk.SampleCountFlagBits
  , anisotropy     :: Float
  , swapchainViews :: Vector Vk.ImageView
  , minImageCount  :: Word32
  , imageCount     :: Word32
  }

-- offscreenTarget offscreenSettings = RenderTarget
--   { surfaceExtent  = Offscreen.sExtent offscreenSettings
--   , surfaceFormat  = Offscreen.sFormat offscreenSettings
--   , depthFormat    = Offscreen.sDepthFormat offscreenSettings
--   , multisample    = Offscreen.sSamples offscreenSettings
--   , anisotropy     = 0
--   , swapchainViews = []
--   , minImageCount  = 1
--   , imageCount     = 1
--   }

instance Vulkan.HasSwapchain RenderTarget where
  getSurfaceExtent  = surfaceExtent
  getSurfaceFormat  = surfaceFormat
  getDepthFormat    = depthFormat
  getMultisample    = multisample
  getAnisotropy     = anisotropy
  getSwapchainViews = swapchainViews
  getMinImageCount  = minImageCount
  getImageCount     = imageCount

setup
  :: ( MonadReader env m
     , HasLogFunc env
     , MonadResource m
     , m ~ RIO env -- FIXME: core:Engine.Setup
     )
  => Options
  -> m Handles
setup opts = do
  (hInstance, hPhysicalDeviceInfo, hPhysicalDevice, hDevice, hAllocator, hQueues) <-
    setupHeadless opts
  pure Headless.Handles{..}
